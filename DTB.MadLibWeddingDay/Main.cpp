// Lab Exercise 3
// Mad Lib ~ Wedding
// Derek Bley

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>  // Added by me to write MadLib result to text file.

using namespace std;

string PrintMadLib(string *madLib);

// START OF APPLICATION.
int main()
{
	// Constant number of MadLib entries.
	const int INPUTS = 10;
	
	// String array.
	string *madLib = new string[INPUTS];

	// Input prompt array.
	string prompts[INPUTS];
	prompts[0] = "Enter a weather adjective (sunny, cloudy, rainy, snowy, etc...): ";
	prompts[1] = "Enter a color: ";
	prompts[2] = "Enter a number: ";
	prompts[3] = "Enter a city: ";
	prompts[4] = "Enter another number: ";
	prompts[5] = "Enter a flavor: ";
	prompts[6] = "Enter a relative (Uncle, Grandma, Cousin, etc...): ";
	prompts[7] = "Enter a song name: ";
	prompts[8] = "Enter a heavy item: ";
	prompts[9] = "Enter your favorite food: ";

	// Loop for inputs.
	for (int i = 0; i < INPUTS; i++)
	{
		cout << prompts[i];
		getline(cin, madLib[i]);
	}

	cout << "\n" << "\n";
	// Print MadLib result.
	cout << PrintMadLib(madLib) << "\n";


	string input;

	// Ask if user wants to save result.
	do
	{
		cout << "Would you like to save this MadLib (y or n)? ";
		cin >> input;
	} while (input != "y" && input != "Y" && input != "n" && input != "N");
	

	if (input == "y")
	{
		// Save text file to path file.
		string path = "C:\\Users\\Public\\WeddingDayMadLib.txt";
		ofstream ofs;
		ofs.open(path);
		ofs << PrintMadLib(madLib);
		ofs.close();
	}
	else

	
	_getch();
	return 0;
}

string PrintMadLib(string *madLib)
{
	string MadLib = "Your wedding day was just the kind of " + madLib[0] + " weather you dreamt about." + "\n"
			+ "The bride's dress was a beautiful " + madLib[1] + "." + "\n"
			+ "Your parents had so many people they wanted to invite.  There were " + madLib[2] + " of people there." + "\n"
			+ "Your father-in-law's speech was mostly about the government in " + madLib[3] + "." + "\n"
			+ "Your mother-in-law kept telling you she wanted " + madLib[4] + " of grand kids." + "\n"
			+ "After dinner, " + madLib[5] + " cake was served." + "\n"
			+ madLib[6] + " drank so much, they had to be taken home." + "\n"
			+ "There is an epic video of Grandpa \"cutting a rug\" to " + madLib[7] + "." + "\n"
			+ "The DJ made everyone try to throw a " + madLib[8] + " across the dance floor.  It was dangerous!" + "\n"
			+ madLib[9] + " was served with the main course for every meal on the honeymoon." + "\n";

	return MadLib;
}